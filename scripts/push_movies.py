#!/usr/bin/python3.7
#
# Script to push data from memory to database
#
import psycopg2


def pushMoviesToDB(movie_array, db_link):
    for movie_group in movie_array:
        for movie in movie_group:
            # FIXME: account for sub-arrays for directors and producers
            #db_link.execute("""INSERT INTO movies VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);""", {"id", "title", "year", "director_id", "producer_id", "cast_id", "studio_id", "process_codes", "categories", "awards", "locale_hierarchy", "notes"})
            
            # db_link.execute("""INSERT INTO movies (id, title, year) VALUES (%(id)s, %(title)s, %(year)s);""", [0, 1, 2])
            try:
                db_link.execute("""INSERT INTO movies (id, title, year) VALUES (%s, %s, %s);""", [movie[0], movie[1], movie[2]])
            except:
                print("Cannot insert")