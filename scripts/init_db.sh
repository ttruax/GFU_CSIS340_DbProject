#!/bin/bash
#
#
###
#
#  This is where you should put any code needed to initialize the database 
#  You probably don't need to change this if you only write sql files located at sql/*
#  Do, however, keep in mind the order that everything gets concatenated
#  
#  Please annotate your sql files
#
###
#
# Default hoststring
#PG_URL=${PG_URL-${1:-postgresql://localhost:5432/jupyter}}
PG_URL=${PG_URL-${1:-postgresql://localhost:5432/csis340}}
#PG_URL=db
#
cat sql/*.sql | psql $PG_URL 
#cat sql/*.sql | psql $PG_URL --user=db