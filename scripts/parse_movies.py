#!/usr/bin/python3.7
#
# Script to import data from XML file 'mains243.xml'
#

import vars

def parse_movies(root):

    # create empty list for movie items 
    movieList = []

    # iterate news items 
    for film in root.findall('films/film'):
        # TODO: add helper method to parse through each 'directorfilm'
        # iterate child elements of film
        # FILM ID
        if ((film.find('t') != None) and (film.find('year') != None) and (str(film.find('year').text).isdigit())):
            film_id = vars.movie_id

            # TITLE
            title = film.find('t').text

            # YEAR
            year = film.find('year').text

            # DIRECTORS
            directors = []
            for director in film.findall('dirs'):
                if (director.find('dir/dirn') != None):
                    directors.append(hash(director.find('dir/dirn').text)) # hash this so we find a unique ID
                    # FIXME: doesn't find additional directors if there is more than one present
            
            # PRODUCERS
            producers = []
            for prod in film.findall('prods'):
                if (prod.find('prod/pname') != None):
                    producers.append(hash(prod.find('prod/pname').text)) # hash for unique id
                    # FIXME: doesn't find additional producers if there is more than one present
            
            # STUDIOS
            studios = []
            for studio in film.findall('studios'):
                if (studio.find('studio') != None):
                    studios.append(hash(studio.find('studio').text))
                    # FIXME: doesn't find additional studios if there is more than one present
            
            # CATEGORIES
            cats = []
            for category in film.findall('cats'):
                if (category.find('cat') != None):
                    cats.append(hash(category.find('cat').text))
                    # FIXME: doesn't find additional categories if there is more than one present

            # AWARDS
            awards = []
            for award in film.findall('awards'):
                if (award.find('award') != None):
                    awards.append(hash(award.find('award').text))
                    # FIXME: doesn't find additional categories if there is more than one present

            # append movie item to movies list 
            
            movieList.append([film_id, title, year, directors, producers, studios, cats, awards])
            # movieList.append({"film_id":film_id, 
            #                     "title":title, 
            #                     "year":year,
            #                     "directors":directors,
            #                     "producers":producers,
            #                     "studios":studios,
            #                     "cats":cats,
            #                     "awards":awards})

        vars.movie_id += 1
    return movieList