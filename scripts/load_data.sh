#!/bin/bash


# This is where you call the ETL program that you write 
# I will guarantee two environment variables when I run it: 
# - PG_URL will be a host string to the database previously loaded using ./scripts/init_db.sh
# - FILE_BASE will be the location of the files to use. Depending on our discussion in class this will be a URL or an absolute file path.



# Make sure pip is installed
sudo apt install -y python-pip

# Install psycopg2 database driver for Python
pip install psycopg2

# initialize the script to load the data from XML
python load_data.py