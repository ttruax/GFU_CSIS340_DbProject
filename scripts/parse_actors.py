#!/usr/bin/python3.7
#
# Script to import data from XML file 'mains243.xml'
#


def parse_actors(root):

    # create empty list for movie items 
    actorsList = []

    # iterate news items 
    for actor in root.findall('actors'):
        # TODO: add helper method to parse through each 'directorfilm'
        # iterate child elements of film

        # STAGENAME
        if (actor.find('stagename') != None):
            stagename = actor.find('stagename').text
        else:
            #TODO: get firstname / lastname before so we can use it here
            stagename = firstname + lastname

        # DOWSTART
        if (actor.find('dowstart') != None):
            dowstart = actor.find('dowstart').text

        # DOWEND
        if (actor.find('dowend') != None):
            dowend = actor.find('dowend').text

        # FAMILYNAME
        if (actor.find('familyname') != None):
            familyname = actor.find('familyname').text
        
        # FIRSTNAME
        if (actor.find('firstname') != None):
            firstname = actor.find('firstname').text

        # GENDER
        if (actor.find('gender') != None):
            gender = actor.find('gender').text

        # DOB
        if (actor.find('dob') != None):
            dob = actor.find('dob').text

        # DOD
        if (actor.find('dod') != None):
            dod = actor.find('dod').text

        # ROLETYPE
        if (actor.find('roletype') != None):
            roletype = actor.find('roletype').text
 
        # append movie item to movies list 
        actorsList.append([stagename, dowstart, dowend, familyname, firstname, gender, dob, dod, roletype])


    return actorsList