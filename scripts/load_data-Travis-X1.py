#!/usr/bin/python3.7
#
# Script to import data from XML into Postgres database
#

# TODO: these imports fail because I need to run the script on the server, not on my machine connecting to the server
# imports
import psycopg2
import xml.etree.ElementTree as ET
from parse_movies import parse_movies
from push_movies import pushMoviesToDB

# variables
from variables import db_name, db_user, db_password, db_host, db_port


def connectToDB():

    # initial connection to database
    try:
        conn = psycopg2.connect(dbname=db_name, user=db_user, password=db_password, host=db_host)
    except:
        print ("Unable to connect to database")

    # TODO: DELETE AFTER TESTING
    conn = psycopg2.connect(dbname=db_name, user=db_user, password=db_password, host=db_host)

    cur = conn.cursor()

    return cur


# TODO: abstract into function 'parseTreeToRoot'
def parseXML(xmlfile):

    # get root of specific level
    level_root = parseTreeToLevel(xmlfile, './directorfilms')
    
    # create empty list for movie items 
    movie_list = []

    # iterate movie items 
    for films in level_root:
        movie_list.append(parse_movies(films))

    # return movie items list 
    return movie_list


# get root from xml file
def parseTreeToLevel(xmlfile, parseLevel):
    return ET.parse(xmlfile).getroot().findall(parseLevel)


def main():

        # connect to the database
        connectToDB()

        # TODO: DELETE AFTER TESTING
        # run one file parser to test
        # TODO: Add all files to list of files to read
        # pushMoviesToDB(parseXML('mains243.xml'), connectToDB())
        print('reached end')










# SAMPLE LOAD
# cur.execute("INSERT INTO movie_list(id) values ('0001ma')")
# cur.execute("INSERT INTO movie_list(name) values ('Die Hard')")

# OTHER SAMPLE LOAD (better?)
# for i in movie(ids)
    # cur.execute("INSERT INTO movie_list(id) values (i)")


# run the main
if __name__ == "__main__":
    main()