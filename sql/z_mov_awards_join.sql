DROP TABLE IF EXISTS mov_awards_join;

CREATE TABLE mov_awards_join (
    id TEXT PRIMARY KEY,
    awards_id TEXT,
    film_id TEXT,
    FOREIGN KEY (awards_id) REFERENCES awards(id),
    FOREIGN KEY (film_id) REFERENCES movies(id)
);