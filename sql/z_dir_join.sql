DROP TABLE IF EXISTS dir_join;

CREATE TABLE dir_join (
    id TEXT PRIMARY KEY,
    dir_id TEXT,
    film_id TEXT,
    FOREIGN KEY (dir_id) REFERENCES people(id),
    FOREIGN KEY (film_id) REFERENCES movies(id)
);