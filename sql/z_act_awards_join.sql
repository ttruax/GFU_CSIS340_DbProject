DROP TABLE IF EXISTS act_awards_join;

CREATE TABLE act_awards_join (
    id TEXT PRIMARY KEY,
    awards_id TEXT,
    actor_id TEXT,
    FOREIGN KEY (awards_id) REFERENCES awards(id),
    FOREIGN KEY (actor_id) REFERENCES actors(id)
);