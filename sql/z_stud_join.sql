DROP TABLE IF EXISTS stud_join;

CREATE TABLE stud_join (
    id TEXT PRIMARY KEY,
    studio_id TEXT,
    film_id TEXT,
    FOREIGN KEY (studio_id) REFERENCES studios(id),
    FOREIGN KEY (film_id) REFERENCES movies(id)
);