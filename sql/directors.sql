DROP TABLE IF EXISTS directors;

CREATE TABLE directors (
    id TEXT PRIMARY KEY, -- referenced by movies
    yr_dir_start INTEGER
);