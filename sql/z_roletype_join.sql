DROP TABLE IF EXISTS roletype_join;

CREATE TABLE roletype_join (
    id TEXT PRIMARY KEY,
    actor_id TEXT,
    roletype_id TEXT,
    FOREIGN KEY (actor_id) REFERENCES actors(id),
    FOREIGN KEY (roletype_id) REFERENCES roletypes(id)
);