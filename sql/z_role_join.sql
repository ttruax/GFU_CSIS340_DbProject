DROP TABLE IF EXISTS role_join;

CREATE TABLE role_join (
    id TEXT PRIMARY KEY,
    actor_id TEXT,
    role_id TEXT,
    FOREIGN KEY (actor_id) REFERENCES actors(id),
    FOREIGN KEY (role_id) REFERENCES roles(id)
);