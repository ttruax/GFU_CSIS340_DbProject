DROP TABLE IF EXISTS studios;

CREATE TABLE studios (
	id TEXT PRIMARY KEY, -- referenced by movies
	name TEXT,
	company TEXT,
	city TEXT,
	country TEXT,
	found_date INTEGER,
	end_date INTEGER,
	founder TEXT,
	successor TEXT
);