DROP TABLE IF EXISTS actors;

CREATE TABLE actors (
	id TEXT PRIMARY KEY,
	stage_name TEXT,
	date_of_start INTEGER,
	date_of_end INTEGER
);