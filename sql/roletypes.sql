DROP TABLE IF EXISTS roletypes;

CREATE TABLE roletypes (
    id TEXT PRIMARY KEY,
    roletype TEXT UNIQUE
);