DROP TABLE IF EXISTS cat_join;

CREATE TABLE cat_join (
    id TEXT PRIMARY KEY,
    cat_id TEXT,
    film_id TEXT,
    FOREIGN KEY (cat_id) REFERENCES categories(id),
    FOREIGN KEY (film_id) REFERENCES movies(id)
);