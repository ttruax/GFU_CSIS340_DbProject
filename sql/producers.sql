DROP TABLE IF EXISTS producers;

CREATE TABLE producers (
    id TEXT PRIMARY KEY, -- referenced by movies
    yr_prod_start INTEGER
);