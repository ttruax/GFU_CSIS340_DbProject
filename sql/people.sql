DROP TABLE IF EXISTS people;

CREATE TABLE people (
    id TEXT PRIMARY KEY,
    given_name TEXT,
    family_name TEXT,
    gender BOOLEAN,
    date_of_birth INTEGER,
    date_of_death INTEGER
);