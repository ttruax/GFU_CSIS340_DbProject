DROP TABLE IF EXISTS prod_join;

CREATE TABLE prod_join (
    id TEXT PRIMARY KEY,
    prod_id TEXT,
    film_id TEXT,
    FOREIGN KEY (prod_id) REFERENCES people(id),
    FOREIGN KEY (film_id) REFERENCES movies(id)
);