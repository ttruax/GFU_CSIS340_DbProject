DROP TABLE IF EXISTS remakes;

CREATE TABLE remakes (
	id TEXT PRIMARY KEY REFERENCES movies(id), -- references movies for the primary key of the remake itself
	fraction DECIMAL,
	og_film_id TEXT REFERENCES movies(id) -- references movies for the id of the movie that was remade
);