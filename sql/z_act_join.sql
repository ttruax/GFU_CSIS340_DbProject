DROP TABLE IF EXISTS act_join;

CREATE TABLE act_join (
    id TEXT PRIMARY KEY,
    actor_id TEXT,
    film_id TEXT,
    FOREIGN KEY (actor_id) REFERENCES people(id),
    FOREIGN KEY (film_id) REFERENCES movies(id)
);